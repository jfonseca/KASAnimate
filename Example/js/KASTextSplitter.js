/*!
 * KAS_TextSpliter: 1.2
 * http://www.klipartstudio.com/
 * GIT: https://gitlab.com/jfonseca
 * 
 * Copyright 2012, Klip Art Studio, LLC
 *
 * BROWSER SUPPORT: Safri 5.0.2, IE8, FF 3.6.10
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 *   name    : Div ID you would like to target
 *   text		     : animate to this x position
 *   type		     : animate to this y position
 *   width     : animate width to this point
 *   height    : animate height to this point
 *   alpha     : animate translucent
 *   color     : Use hex value to define color (used for text and other items that use color)
 *   BGcolor   : Use hex value to define color (this to specify background color of items)
 *   speed     : How fast till it is done animating
 *   delay     : How long to wait before it starts animating
 *   ease      : ease into place 'backeaseOut','bounceEaseOut','circEaseOut','cubicEaseOut','elasticEaseOut','expoEaseOut','linearEaseOut','quadEaseOut','quartEaseOut'
 *   onComplete: Call a function once it is complete
 *
 * 	 CSS3 Params Option (CSS3:true) must be set:
 *   		scale   : 1.0 is 100 percent of current item
 *   		rotate  : Rotation of item
 *
 *
 *  
 * USAGE:
 *		var items = new kas.Animate();
 * 		items.Tween(this, {target:'divID',y:randRange(10, 500), x:randRange(10, 500), duration:1, ease:'linearEaseOut', onComplete:'finishedFunction', alpha:0, delay:.1});
 *
 * CSS3 Option:
 *		var itemsCSS3 = new kas.Animate();
 * 		itemsCSS3.Tween(this, {target:'divID',y:randRange(10, 500), x:randRange(10, 500), duration:1, ease:'linearEaseOut', onComplete:'finishedFunction', alpha:0, delay:.1, CSS3:true, scale:scale, rotate:rotation});
 *
 * Timer Option:
 *		var timer = new kas.Animate();
 * 		timer.Tween(this, {duration:5, timer:contiousFunctionCall, onComplete:finishedFunction});
 *
 * AUTHOR: Jose Fonseca, josef@klipartstudio.com
 * DATE: 11/11/2011
 */
var tfx = {};
tfx.textFX = function(scope, p_parameters) {
  this.p_obj = p_parameters;
  this.animateTxt = "I Love Code!";
  this.textArr = [];
  this.lettersArr = [];
  this.spitType = new Array("", " ");
  this.tempX = 0;
  this.tempY = 0;
  this.params = {};
  this.type = p_parameters.type ? this.typeCheck(p_parameters.type) : 0;
  this.css3 = false;
  this.count = 0;
  this.onComplete = null;
  this.ease = "linearEaseInOut";
  this.scope = scope;
  this.nextXpos = 0;
  this.nextYpos = 0;
  this.animateTxt = p_parameters.text ? p_parameters.text : this.animateTxt;

  console.log("Tween COMPLETE");
  this.build();
  this.getArray();
  return this;
};

tfx.textFX.prototype.Tween = function(scope, p_parameters) {
  for (var prop in p_parameters) {
    console.log(prop + " = " + p_parameters[prop]);
  }
  this.animateTxt = p_parameters.text;

  console.log("Tween COMPLETE");
  return this.build();
};
tfx.textFX.prototype.typeCheck = function(check) {
  var typeId = 0;

  if (check == "words" || check == 1) {
    typeId = 1;
  }
  return typeId;
};
tfx.textFX.prototype.getArray = function() {
	return this;
  };
tfx.textFX.prototype.addElement = function(letter) {
  var fxDiv = document.getElementById("textFX");
  var newdiv = document.createElement("div");

  var divIdName = "my" + this.count + "Div";

  newdiv.setAttribute("id", divIdName);
  newdiv.setAttribute("class", "ttFX");

  if (letter == " ") {
    newdiv.innerHTML = '<span class="fxSpace">&nbsp;<span>';
  } else {
    newdiv.innerHTML = letter;
  }

  var split_div = fxDiv.appendChild(newdiv);

  //var item = document.getElementById(divIdName);
  newdiv.style.opacity = 0;
  newdiv.style.filter = "alpha(opacity=" + 0 + ")";

  this.lettersArr[this.count] = {
    base: fxDiv,
    name: divIdName,
    target: newdiv,
    ogX: this.nextXpos,
    ogY: this.nextYpos,
    x: 0,
    y: 0,
    alpha: 0
  };

  this.nextXpos += newdiv.offsetWidth;
};

/* var spitType = new Array("", " "); */
tfx.textFX.prototype.build = function() {
  this.textArr = [];
  this.lettersArr = [];
  this.count = 0;

  var fxDiv = document.getElementById("textFX");
  if (typeof(this.p_obj.target) == "object") {
    fxDiv = this.p_obj.target;
  } else {
    fxDiv = document.getElementById(this.p_obj.target);
  }
  fxDiv.innerHTML = "";
  var splitKind = this.typeCheck(this.type);

  if (splitKind > 0) {
    this.textArr = this.animateTxt.split(this.spitType[splitKind]);
    var total = this.textArr.length - 1;
    var temp = [];
    for (i = 0; i <= total; i++) {
      temp.push(this.textArr[i]);
      temp.push(" ");
    }
    this.textArr = temp;
  } else {
    this.textArr = this.animateTxt.split(this.spitType[splitKind]);
  }
  for (i = 0; i <= this.textArr.length - 1; i++) {
    this.addElement(this.textArr[i]);
    this.count += 1;
  }
  //this.tweenLetters();
  console.log("BUILD COMPLETE");
  if (typeof(this.p_obj.onComplete) == "function") {
    this.p_obj.onComplete(this);
  } else if (typeof(this.p_obj.onComplete) == "string") {
    window[this.p_obj.onComplete](this);
  }
  return this.lettersArr;
};
