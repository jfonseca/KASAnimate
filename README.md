# KASAnimate 1.2

This animation library was inspired by Jack Doyle library for flash called GreenSock. I started my journey developing this library after I sent Jack Doyle an email around 2003 asking if he had any plans to port Greensock library to Javascript. He replied “he had no plans in moving Greensock over to Javascript at that time”. Then came 2012 Jack made a huge announcement that GSAP Javascript version is out. I was relieved by the announcement due to Jack had years developing this library and included many features I loved like TimelineLite and TimelineMax! Cheers to Jack for standing with us through the transition(AS3 to JS). 

Now go checkout [Greensock] https://greensock.com

## Example

To run the examples you can either upload 'Example' folder to your server or run it locally by using the npm library inside the 'Example' folder.

[View Example Online](https://www.klipartstudio.com/site/KAS_TextSplitter_AnimateJS/)

[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it

* Install the package from npm

`npm install`

* Start the local server

`npm start`

## Usage

* KASUtils.js required

### DESCRIPTION:

* target    : Div ID you would like to target
* x		     : animate to this x position
* y		     : animate to this y position
* width     : animate width to this point
* height    : animate height to this point
* alpha     : animate translucent
* startX	    : Start animation x position
* startY		: Start animation y position
* startAlpha : Start animation translucent position
* startWidth : Start animation width position
* startHeight: Start animation height position
* color     : Use hex value to define color (used for text and other items that use color)
* BGcolor   : Use hex value to define color (this to specify background color of items)
* speed     : How fast till it is done animating
* delay     : How long to wait before it starts animating
* ease      : ease into place 'backeaseOut','bounceEaseOut','circEaseOut','cubicEaseOut','elasticEaseOut''expoEaseOut','linearEaseOut','quadEaseOut','quartEaseOut'
* onComplete: Call a function once it is complete
 
 CSS3 Params Option (CSS3:true) must be set:

 * scale      : 1.0 is 100 percent of current item
 * rotate     : Rotation of item
 * startScale : Start animation Scale

Javascript code:

```javascript

//General Use
var items = new kas.Animate();
items.Tween(this, {target:'divID',y:randRange(10, 500), x:randRange(10, 500), duration:1, ease:'linearEaseOut', onComplete:'complete', alpha:0, delay:.1});

// CSS3 Option:
var itemsCSS3 = new kas.Animate();
itemsCSS3.Tween(this, {target:'divID',y:randRange(10, 500), x:randRange(10, 500), duration:1, ease:'linearEaseOut', onComplete:'finishedFunction', alpha:0, delay:.1, CSS3:true, scale:scale, rotate:rotation});

// Timer Option:
var timer = new kas.Animate();
timer.Tween(this, {duration:5, timer:contiousFunctionCall, onComplete:finishedFunction});

//Optional
function complete(obj) {
    console.log('Complete element  ', obj);
}
```

## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASAudioVideoPlayer)

## License

MIT
